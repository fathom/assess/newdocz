export const imports = {
  'index.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "index" */ 'index.mdx'),
  'pages/protocol.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "pages-protocol" */ 'pages/protocol.mdx'
    ),
  'pages/token.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "pages-token" */ 'pages/token.mdx'
    ),
  'pages/vision.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "pages-vision" */ 'pages/vision.mdx'
    ),
}
