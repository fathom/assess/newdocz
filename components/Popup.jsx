import React from "react";
import Popup from "reactjs-popup";
import styled from "styled-components"

export const TriggerText = styled('span')`
color: blue;
`

const sampleText = {
  text: 'a weird word',
  explanation: 'it is weirdopian for a totally regular word',
  position: 'top-center'
}

// const positions = ['top center', 'bottom center', 'top left', 'bottom left']
const positions = ['bottom center']

const getRandomPosition = () => {
 let tmp = positions[Math.floor(Math.random() * positions.length)]
  console.log('random position is ', tmp )
  return tmp
}

const PU = (props) => {
  return (
    <Popup
      trigger={<TriggerText> {props.text} </TriggerText>}
      position={getRandomPosition()}
    closeOnDocumentClick
  >
    <div >
      {props.explanation}
      {/* <Popup */}
      {/*   trigger={<button className="button"> Trigger 2 </button>} */}
      {/*   position="bottom left" */}
      {/*   closeOnDocumentClick */}
      {/* > */}
      {/*   <div> */}
      {/*     Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores */}
      {/*     dolor nulla animi, natus velit assumenda deserunt */}
      {/*     <Popup */}
      {/*       trigger={<button className="button"> Trigger 3 </button>} */}
      {/*       position="top right" */}
      {/*       closeOnDocumentClick */}
      {/*     > */}
      {/*       <span> Popup content </span> */}
      {/*     </Popup> */}
      {/*   </div> */}
      {/* </Popup> */}
    </div>
  </Popup>
  )
}

export default PU;
