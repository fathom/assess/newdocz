import React from 'react'
import Tooltip from 'react-simple-tooltip'
import {TriggerText} from './Popup.jsx'
import {css} from 'styled-components'
// import h from 'hyperscript' // does not work


const Tt = (props) => (
  <Tooltip
    content={props.explanation}
    background='#eee'
    color='#111'
    customCss={css`
      white-space: nowrap;
    `}
  >
    <TriggerText>{props.text}</TriggerText>
  </Tooltip>
)

export default Tt;
